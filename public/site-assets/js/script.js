
/* Language Tradutor */
const languageData = {
    'en': {
        'Location': '<strong>Location:</strong>',
        'Portuguese': 'Portuguese',
        'English': 'English',
        'Spanish': 'Spanish',
        'GetInTouch': 'Get in touch',
        'AboutMe': 'ABOUT ME',
        'Hello': "Hello, I'm José Rocha, and my professional journey began in 2014. Since then, I have undertaken various roles, continuously growing my expertise. Passionate learner, sports enthusiast, and tech-savvy. Embracing the world of electronics with curiosity and enthusiasm.",
        'Experience': 'EXPERIENCE',
        'TeamLeader': 'Team Leader - Jul 2022 - Dec 2022 <span class="location">V. N. Gaia</span>',
        'Coordinated': 'Coordinated teams to effectively engage with customers, gaining their trust and providing a unique in-store experience. Trained the team to foster collaboration, focus, and results-oriented approaches. Conducted analyses of analytical data and business strategies to achieve better outcomes. Oversaw fiscal inventory control.',
        'UsedSoftware': 'Used Software:',
        'ContentEditor': 'Content Editor - Mar 2022 - May 2022 <span class="location">Aveiro</span>',
        'Developed': 'Developed market analyses and benchmarks. Implemented current trends and devised strategies to anticipate future ones. Ensured the website remains user-friendly for consumers. Created and updated product data sheets.',
        'Salesperson': 'Specialized Store Salesperson - Oct 2014 - Mar 2022 <span class="location">Aveiro</span>',
        'Customers': 'Delivered a unique in-store experience to customers, ensuring their comfort and willingness to return. Strived for win-win situations, aligning customer needs with company objectives. Results-oriented with responsibilities including store stock control, participation in fiscal inventories, and sales management.',
        'WortenSpain': 'WORTEN SPAIN:',
        'Trainer': 'Trainer - Mar 2021 - Apr 2021 <span class="location">Madrid, S. S. Reyes</span>',
        'Manage': "Managed teams and trained new members to become customer-focused sellers while maintaining the company's objectives in mind. Provided instruction on store stock control and implemented merchandising plans to captivate customer interest in acquiring new products.",
        'Blogger': 'Freelance Blogger - Sep 2016 - Feb 2018',
        'GeekLoft':'As a hobby during this period, I created a website called "TheGeekLoft.pt" to introduce people to the world of video games, including personal experiences from the team. Covered everything from major industry title releases to indie games and startups.',
        'Education': 'EDUCATION',
        'UdemyCourseOne': 'Complete Web Development - 2023',
        'UdemyDescription': 'In order to enter the world of programming, I took several courses on Udemy to get acquainted with some of the technologies used and gain the skills necessary to enter the workforce. The "<a target="_blank" href="https://www.udemy.com/course/web-completo/" class="beige">Complete Web Development course</a>", as the name suggests, was the most comprehensive and allowed me to get familiar with some of the most commonly used programming languages today.',
        'UdemyCourseTwo': 'Coding for Beginners - 2023',
        'UdemyDescriptionTwo': 'I attended the courses "<a target="_blank" href="https://www.udemy.com/course/coding-for-beginners-you-can-learn-to-code/" class="beige">Coding for Beginners 1</a>" and "<a target="_blank" href="https://www.udemy.com/course/coding-for-beginners-2-get-started-with-web-development/" class="beige">Coding For Beginners 2</a>" where I embarked on my journey of learning programming.',
        'Polytechnic': 'Polytechnic Institute of Bragança - ESACT - 2010 - 2014 <span class="location">Aveiro</span>',
        'DigitalGame': 'Attended the Digital Game Design course, where I learned about the entire game creation process and its various aspects. I was particularly fascinated by character creation and development. I also delved into programming, working with Unity and technologies like C and C++.',
        'Certificate': 'Teaching Skills Certificate - 2016 <span class="location">Aveiro</span>',
        'Pedagogical': 'Obtained a certificate in pedagogical competencies (Teaching Skills), enabling me to be a trainer. This course provided me with the ability to adapt to different organizational contexts, plan and prepare training sessions, facilitate and mediate learning processes, and evaluate training efficiency, among other skills.',
        'HighSchool': 'High School - José Estêvão - 2008 - 2010 <span class="location">Aveiro</span>',
        'Arts': 'Completed my High School in the Arts course. In addition to common classes found in other courses, this course had a particular focus on classes in Drawing, Geometry, and Manual Arts Workshops.',
        'OtherProjects': 'OTHER PROJECTS',
        'Goodies': {
            text: 'Recently, I created "<a target="_blank" href="https://www.etsy.com/shop/Goodie4Gamers" class="shop-link beige">Goodies 4 Gamers</a>", a digital store for selling vectorized items. Using Illustrator, I design simple graphics related to games, TV series, and movies, which customers can acquire for use in shirt printing, sticker creation, cutting machines like Cricut or Silhouette, MDF cutting, and various other applications. This keeps my designer side active and thriving.',
            link: 'https://www.etsy.com/shop/Goodie4Gamers'
        },
        'Future': 'In the near future, I plan to add t-shirts with designs created by me and stickers to this project.',
        'UsedTecnologie': 'Used Technologies',
        'School': 'School education',
        'Programing': 'Programing',
        'Trainer2': 'Trainer',
        'SSkills': '<strong>Soft Skills</strong>',
        'Detail-Oriented': 'Detail-Oriented',
        'Teamwork': 'Teamwork',
        'Adaptability': 'Adaptability',
        'Commitment':'Commitment',
        'Resilient': 'Resilient',
        'Empathic': 'Empathic',
        'TecSkills': 'Technical Skills',
        },
    'pt': {
        'Location': '<strong>Localização:</strong>',
        'Portuguese': 'Português',
        'English': 'Inglês',
        'Spanish': 'Espanhol',
        'GetInTouch': 'Entrar em contacto',
        'AboutMe': 'SOBRE MIM',
        'Hello': 'Olá, sou o José Rocha e a minha jornada profissional começou em 2014. Desde então, tenho assumido várias funções, continuamente aprimorando a minha experiência. Apaixonado por aprender, entusiasta do desporto e adepto da tecnologia. Abraçando o mundo da eletrónica com curiosidade e entusiasmo.',
        'Experience': 'EXPERIÊNCIA',
        'TeamLeader': 'Coordenador de Equipas - Jul 2022 - Dez 2022 <span class="location">V. N. Gaia</span>',
        'Coordinated': 'Coordenava equipas para interagir eficazmente com os clientes, ganhando a sua confiança e proporcionando uma experiência única na loja. Treinava a equipa para fomentar a colaboração, foco e abordagens orientadas para resultados. Realizava análises de dados analíticos e estratégias de negócio para cumprir e procurar ultrapassar os objectivos. Supervisionava o controlo de inventário fiscal.',
        'UsedSoftware': 'Software Utilizado:',
        'ContentEditor': 'Editor de Conteúdos - Mar 2022 - Mai 2022 <span class="location">Aveiro</span>',
        'Developed': 'Elaborava análises de mercado e benchmarks. Implementava tendências atuais e desenvolvia estratégias para antecipar as futuras. Garantia que o website mantinha a fácil interação para os consumidores. Criava e atualizava fichas técnicas dos produtos.',
        'Salesperson': 'Vendedor Especializado - Out 2014 - Mar 2022 <span class="location">Aveiro</span>',
        'Customers': 'Procurava Proporcionar uma experiência única aos clientes na loja, garantindo o seu conforto e vontade de regressar. Procurava alcançar situações vantajosas para ambas as partes, alinhando as necessidades dos clientes com os objetivos da empresa. Orientado para resultados, com responsabilidades que incluíam controlo de stocks, participação em inventários fiscais e gestão de vendas.',
        'WortenSpain': 'WORTEN ESPANHA:',
        'Trainer': 'Formador - Mar 2021 - Abr 2021 <span class="location">Madrid, S. S. Reyes</span>',
        'Manage': 'Geria equipas e formava novos membros para se tornarem vendedores focados na satisfação do cliente, mantendo sempre os objetivos da empresa em mente. Fornecia instrução sobre controlo de stocks da loja e implementava planos de merchandising para cativar o interesse dos clientes em adquirir novos produtos.',
        'Blogger': 'Blogger Freelance - Set 2016 - Fev 2018',
        'GeekLoft':'Durante este período, criei o website "TheGeekLoft.pt" como um hobby para introduzir as pessoas ao mundo dos videojogos, incluindo experiências pessoais. Cobria desde os principais lançamentos da indústria até jogos indie e startups.',
        'Education': 'EDUCAÇÃO',
        'UdemyCourseOne': 'Desenvolvimento Web Completo - 2023',
        'UdemyDescription': 'Com o objetivo de entrar no mundo da programação, fiz vários cursos na Udemy para familiarizar-me com algumas das tecnologias utilizadas e adquirir habilidades essenciais para poder ingressar no mercado de trabalho. O "<a target="_blank" href="https://www.udemy.com/course/web-completo/" class="beige">Curso de Desenvolvimento Web Completo</a>", como o próprio nome indica, foi o mais abrangente e permitiu-me familiarizar com algumas das linguagens de programação mais utilizadas nos dias de hoje.',
        'UdemyCourseTwo': 'Coding for Beginners - 2023',
        'UdemyDescriptionTwo': 'Frequentei os cursos "<a target="_blank" href="https://www.udemy.com/course/coding-for-beginners-you-can-learn-to-code/" class="beige">Coding for Beginners 1</a>" e "<a target="_blank" href="https://www.udemy.com/course/coding-for-beginners-2-get-started-with-web-development/" class="beige">Coding For Beginners 2</a>" onde iniciei a minha jornada de aprendizagem em programação.',
        'Polytechnic': 'Polytechnic Institute of Bragança - ESACT - 2010 - 2014 <span class="location">Aveiro</span>',
        'DigitalGame': 'Frequentei o curso de Design de Jogos Digitais, onde aprendi sobre todo o processo de criação de jogos nas suas várias vertentes. O que mais me fascinou foi a criação e desenvolvimento de personagens. Também me iniciei na programação, trabalhando com Unity e tecnologias como C e C++.',
        'Certificate': 'Certificado de Competências Pedagógicas - 2016 <span class="location">Aveiro</span>',
        'Pedagogical': 'Obtive um certificado em competências pedagógicas, permitindo-me ser formador. Este curso proporcionou-me a capacidade de me adaptar a diferentes contextos organizacionais, planificar e preparar sessões de formação, facilitar e mediar processos de aprendizagem, avaliar a eficácia da formação, entre outras competências.',
        'HighSchool': 'Escola Secundária José Estêvão - 2008 - 2010 <span class="location">Aveiro</span>',
        'Arts': 'Completei o ensino secundário no curso de Artes. Além das disciplinas comuns a outros cursos, este teve um foco particular em aulas de desenho, Geometria Descritiva e Oficina de Artes Manuais.',
        'OtherProjects': 'OUTROS PROJETOS',
        'Goodies': {
            text: 'Recentemente, criei "<a target="_blank" href="https://www.etsy.com/shop/Goodie4Gamers" class="shop-link beige">Goodies 4 Gamers</a>", uma loja digital para venda de artigos vectorizados. Utilizando o Illustrator, desenvolvo designs simples relativos a jogos, séries e filmes que os clientes podem adquirir para utilizar em estampagem de t-shirts, criação de stickers, máquinas de corte como Cricut ou Silhouette, corte em MDF entre outras aplicações. Desta forma, mantenho o meu lado de designer ativa e em crescimento.',
            link: 'https://www.etsy.com/shop/Goodie4Gamers'
        },
        'Future': 'No futuro próximo, pretendo adicionar t-shirts com designs criados por mim e autocolantes a este projeto.',
        'UsedTecnologie': 'Tecnologias usadas',
        'School': 'Educação escolar',
        'Programing': 'Programação',
        'Trainer2': 'Formador',
        'SSkills': '<strong>Competências Transversais</strong>',
        'Detail-Oriented': 'Orientado para Detalhes',
        'Teamwork': 'Trabalho de Equipa',
        'Adaptability': 'Adaptabilidade',
        'Commitment':'Compromisso',
        'Resilient': 'Resiliente',
        'Empathic': 'Empatia',
        'TecSkills': 'Competências Técnicas',
    }
};


/* Language Translation */
function translateElements(lang) {
    const elements = document.querySelectorAll('[data-translate]');
    elements.forEach(element => {
        const key = element.dataset.translate;
        if (languageData[lang][key]) {
            if (typeof languageData[lang][key] === 'string') {
                element.innerHTML = languageData[lang][key];
            } else if (typeof languageData[lang][key] === 'object') {
                element.innerHTML = languageData[lang][key].text;
                if (element.tagName === 'A') {
                    element.setAttribute('href', languageData[lang][key].link);
                }
            }
        }
    });
}


/* Language Selection */
function setActiveButton(lang) {
    if (lang === 'pt') {
        document.getElementById('btn-pt').classList.add('btn-beige');
        document.getElementById('btn-pt').classList.remove('btn-grey');
        document.getElementById('btn-en').classList.add('btn-grey');
        document.getElementById('btn-en').classList.remove('btn-beige');
    } else if (lang === 'en') {
        document.getElementById('btn-en').classList.add('btn-beige');
        document.getElementById('btn-en').classList.remove('btn-grey');
        document.getElementById('btn-pt').classList.add('btn-grey');
        document.getElementById('btn-pt').classList.remove('btn-beige');
    }
}

document.getElementById('btn-pt').addEventListener('click', () => {
    translateElements('pt');
    setActiveButton('pt');
});

document.getElementById('btn-en').addEventListener('click', () => {
    translateElements('en');
    setActiveButton('en');
});


/* Remove grid class from Skills */
function removeGridContainer() {
    var div = document.getElementById('skill');
    if (window.innerWidth >= 200 && window.innerWidth <= 1024) {
      div.classList.remove('grid-container');
    } else {
      div.classList.add('grid-container');
    }
}
  

window.onload = function() {
    removeGridContainer();
};

window.addEventListener('resize', function() {
    removeGridContainer();
});

/* Remove flex class from Technical Skills */
function removeFlexTecSkill() {
    var div = document.getElementById('teck-skill');
    if (window.innerWidth >= 200 && window.innerWidth <= 1024) {
      div.classList.remove('flex-container');
    } else {
      div.classList.add('flex-container');
    }
}
  

window.onload = function() {
    removeFlexTecSkill();
};

window.addEventListener('resize', function() {
    removeFlexTecSkill();
});




/* Call Functions */
translateElements('en');
setActiveButton('en');
